/* global Ext, data, Lang */

Ext.define('model.rule', {
   extend: 'Ext.data.Model',
   idProperty: 'id',
   fields: [
      {name: 'id', type: 'int'},
      {name: 'name', type: 'string'},
      {name: 'type', type: 'string'},
      {name: 'operators'},
      {name: 'values'}
   ]
});

Ext.define('model.operator', {
   extend: 'Ext.data.Model',
   idProperty: 'id',
   fields: [
      {name: 'id', type: 'int'},
      {name: 'name', type: 'string'}
   ]
});

Ext.define('model.values', {
   extend: 'Ext.data.Model',
   idProperty: 'key',
   fields: [
      {name: 'key', type: 'int'},
      {name: 'value', type: 'string'}
   ]
});

Ext.define("store.rule", {
   extend: 'Ext.data.Store',
   model: 'model.rule',
   proxy: {
      type: 'memory',
      reader: {
         type: 'json'
      }
   },
   data: data.rules
});

Ext.create("store.rule", {
   storeId: 'store-rule'
});

Ext.define("combo.operator", {
   extend: 'Ext.form.ComboBox',
   alias: 'widget.combo-operator',
   fieldLabel: '',
   emptyText: '',
   value: '',
   displayField: 'name',
   valueField: 'id',
   multiSelect: false,
   editable: false,
   constructor: function (config) {
      var me = this;

      me.initConfig(extend({
         data: []
      }, config));

      me.store = Ext.create('Ext.data.Store', {
         storeId: new Date().getTime().toString(),
         model: 'model.operator',
         data: me.config.data,
         proxy: {
            type: 'memory',
            reader: {
               type: 'json'
            }
         }
      });

      me.callParent([config]);
   },
   listeners: {
      'select': function (combo, records, eOpts) {
         if (count(records) === 1) {
            log(records[0].data);
         }
      },
      'afterrender': function (combo, eOpts) {
         var value = combo.getValue();
         if (!Ext.isEmpty(value)) {
            var record = combo.getStore().getById(value);
            log("combo_operator#afterrender", record);
            if (!record)
               combo.setValue('');
         }
      }
   }
});

Ext.define("combo.values", {
   extend: 'Ext.form.ComboBox',
   alias: 'widget.combo-values',
   fieldLabel: '',
   emptyText: '',
   value: '',
   displayField: 'value',
   valueField: 'key',
   multiSelect: false,
   editable: false,
   constructor: function (config) {
      var me = this;

      me.initConfig(extend({
         data: []
      }, config));

      me.store = Ext.create('Ext.data.Store', {
         storeId: new Date().getTime().toString(),
         model: 'model.values',
         proxy: {
            type: 'memory',
            reader: {
               type: 'json'
            }
         },
         data: me.config.data
      });

      me.callParent([config]);
   },
   listeners: {
      'afterrender': function (combo, eOpts) {
         var value = combo.getValue();
         if (!Ext.isEmpty(value)) {
            var record = combo.getStore().getById(value);
            log("combo_value#afterrender", record);
            if (!record)
               combo.setValue('');
         }
      }
   }
});

Ext.define('panel.rule', {
   extend: 'Ext.panel.Panel',
   alias: 'widget.panel-rule',
   layout: {
      type: 'hbox',
      pack: 'start',
      align: 'stretch'
   },
   border: false,
   bodyBorder: false,
   headerVisible: false,
   bodyPadding: 4,
   bodyStyle: "background:#DFE8F6",
   cls: 'x-panel-rule',
   setLastRule: function () {
      Ext.getCmp(this.props.btnLess).disable();
   },
   listeners: {
      'afterrender': function (panel) {
         setTimeout(function () {
            panel.setBodyStyle('background', '#FFFFFF');
         }, 200);
      }
   },
   constructor: function (config) {

      var me = this;

      me.initConfig(extend({
         counter: 0
      }, config));

      log(me.config);

      me.id = 'panel-rule-' + me.config.counter;

      me.props = {
         cmbRule: 'cmb-rule',
         cmbOperator: 'cmb-operator',
         btnPlus: 'btn-plus',
         btnLess: 'btn-less',
         panelValues: 'pnl-values',
         panelOperator: 'pnl-operator',
         valueObj: 'obj-value'
      };

      for (var i in me.props)
         me.props[i] = me.props[i] + "-" + me.config.counter;

      me.refresh = function (rule) {
         if (rule) {
            Ext.getCmp(me.props.panelOperator).updateField(rule.get('operators'));
            Ext.getCmp(me.props.panelValues).updateFieldByType(rule.get('type'), rule.get('values'));
         }
      };

      me.items = [{
            xtype: 'panel',
            border: false,
            bodyBorder: false,
            headerVisible: false,
            layout: {
               type: 'fit',
               pack: 'start',
               align: 'stretch'
            },
            flex: 0.1,
            margin: 2,
            items: {
               xtype: 'label',
               text: me.config.counter + "."
            }
         }, {
            xtype: 'panel',
            border: false,
            bodyBorder: false,
            headerVisible: false,
            layout: {
               type: 'fit',
               pack: 'start',
               align: 'stretch'
            },
            flex: 1.5,
            margin: 2,
            items: {
               xtype: 'combobox',
               id: me.props.cmbRule,
               fieldLabel: '',
               emptyText: '',
               value: me.config.item.idRule,
               displayField: 'name',
               valueField: 'id',
               multiSelect: false,
               editable: false,
               store: Ext.data.StoreManager.lookup('store-rule'),
               listeners: {
                  'select': function (combo, records, eOpts) {
                     if (count(records) > 0)
                        me.refresh(records[0]);
                  },
                  'afterrender': function (combo, eOpts) {
                     var value = combo.getValue();
                     if (!Ext.isEmpty(value))
                        me.refresh(combo.getStore().getById(value));
                  }
               }
            }
         }, {
            xtype: 'panel',
            layout: {
               type: 'fit',
               pack: 'start',
               align: 'stretch'
            },
            id: me.props.panelOperator,
            flex: 1.5,
            margin: 2,
            border: false,
            bodyBorder: false,
            headerVisible: false,
            updateField: function (data) {
               this.removeAll();
               this.insert(0, {
                  xtype: 'combo-operator',
                  id: me.props.cmbOperator,
                  data: data || [],
                  value: me.config.item.idOperator
               });
            },
            items: {
               xtype: 'combo-operator',
               id: me.props.cmbOperator,
               value: me.config.item.idOperator
            }
         }, {
            xtype: 'panel',
            id: me.props.panelValues,
            flex: 2,
            margin: 2,
            border: false,
            bodyBorder: false,
            headerVisible: false,
            layout: {
               type: 'fit',
               pack: 'start',
               align: 'stretch'
            },
            updateFieldByType: function (type, data) {
               var field = field = {
                  xtype: 'textfield',
                  flex: 1,
                  id: me.props.valueObj,
                  value: me.config.item.value
               };
               log("xxx", field);
               if (type === 'COMBOBOX') {
                  field.xtype = 'combo-values';
                  field.data = data || [];
               }
               this.removeAll();
               this.insert(0, field);
            },
            items: {
               flex: 1,
               xtype: 'textfield',
               value: me.config.item.value
            }
         }, {
            xtype: 'panel',
            flex: 0.2,
            margin: 2,
            border: false,
            bodyBorder: false,
            headerVisible: false,
            items: {
               xtype: 'button',
               text: '+',
               id: me.props.btnPlus,
               width: 22,
               style: 'margin-left:2px',
               handler: function () {
                  Ext.getCmp(me.config.parentId).add_rule(me);
                  Ext.getCmp(me.props.btnLess).enable();
               }
            }
         }, {
            xtype: "panel",
            flex: 0.2,
            margin: 2,
            border: false,
            bodyBorder: false,
            headerVisible: false,
            items: {
               xtype: 'button',
               text: '-',
               id: me.props.btnLess,
               width: 22,
               disabled: me.config.counter === 0,
               handler: function () {
                  Ext.getCmp(me.config.parentId).remove_rule(me.id);
               }
            }
         }];

      me.callParent([config]);
   }
});

Ext.create('Ext.data.Store', {
   storeId: 'status-filter-store',
   fields: ['status', 'name'],
   data: [
      {"status": 1, "name": Lang.get("label.active")},
      {"status": 0, "name": Lang.get("label.notActive")}
   ]
});

Ext.define('panel.rules', {
   extend: 'Ext.form.Panel',
   alias: 'widget.panel-rules',
   counters: 0,
   layout: {
      type: "vbox",
      pack: 'start',
      align: 'stretch'
   },
   border: false,
   bodyBorder: false,
   headerVisible: false,
   height: 400,
   width: 710,
   defaults: {bodyPadding: 4},
   constructor: function (config) {

      var me = this, UID = new Date().getTime().toString();

      me.initConfig(extend({
      }, config));

      me.props = {
         panelRules: 'panel-rules'
      };

      for (var i in me.props)
         me.props[i] = me.props[i] + "-" + UID;

      me.items = [{
            xtype: 'hiddenfield',
            name: "idFilter"
         }, {
            xtype: 'panel',
            border: false,
            layout: "vbox",
            defaults: {
               flex: 1
            },
            items: [{
                  xtype: 'textfield',
                  fieldLabel: Lang.get('label.filterName'),
                  name: 'name',
                  maxLength: 256
               }, {
                  xtype: 'combobox',
                  name: 'status',
                  fieldLabel: Lang.get("label.status"),
                  emptyText: '',
                  value: 1,
                  displayField: 'name',
                  valueField: 'status',
                  multiSelect: false,
                  editable: false,
                  store: Ext.data.StoreManager.lookup('status-filter-store')
               }]
         }, {
            xtype: 'panel',
            labelAlign: 'top',
            defaultType: 'checkbox',
            border: false,
            defaults: {
               flex: 1,
               style: 'margin-left:20px'
            },
            layout: 'vbox',
            items: [{
                  xtype: 'label',
                  text: Lang.get('label.whenApplyFilter'),
                  style: 'margin-left:0px'
               }, {
                  boxLabel: Lang.get('label.downloadNewMail'),
                  name: 'apply_download',
                  inputValue: '1'
               }, {
                  boxLabel: Lang.get('label.afterSent'),
                  name: 'apply_after_sent',
                  inputValue: '1'
               }]
         }
         , {
            xtype: 'panel',
            layout: {
               type: "column"
            },
            border: false,
            defaultType: 'radiofield',
            defaults: {
               flex: 1
            },
            items: [{
                  boxLabel: Lang.get("label.andCondition"),
                  name: 'condition',
                  inputValue: '0',
                  columnWidth: .25
               }, {
                  boxLabel: Lang.get("label.orCondition"),
                  name: 'condition',
                  inputValue: '1',
                  columnWidth: .35
               }, {
                  boxLabel: Lang.get("label.allCondition"),
                  name: 'condition',
                  inputValue: '2',
                  columnWidth: .25

               }]
         }, {
            xtype: 'panel',
            id: me.props.panelRules,
            border: false,
            headerVisible: false,
            //autoScroll: true,
            overflowY: 'scroll',
            height: 200
         }];

      me.callParent([config]);
   },
   reconfigure: function () {

      var children = Ext.getCmp(this.props.panelRules).items.items;

      if (count(children) > 0) {

         for (var i = 0; i < count(children); i++)
            children[i].position = i;

         if (count(children) === 1)
            children[0].setLastRule();
      }
   },
   add_rule: function (beforeCmp, rule) {

      var me = this,
              position = 0;
      rule = rule || {};

      if (beforeCmp) {
         position = beforeCmp.position + 1;
      } else {
         // put in coda
         var children = Ext.getCmp(me.props.panelRules).items.items;
         if (count(children) > 0) {
            position = children[count(children) - 1].position + 1;
         }
      }

      log("panel.rules#add_rule rule = {");
      log(rule);
      log("}")
      log("panel.rules#add_rule [counters=" + me.counters + ", position=" + position + "]");

      var newID = me.counters++;
      log("panel.rules#add_rule [newID=" + newID + "]");

      Ext.getCmp(me.props.panelRules).insert(position, {
         xtype: 'panel-rule',
         counter: newID,
         position: position,
         parentId: me.getId(),
         item: extend({
            idRule: '',
            idOperator: '',
            value: ''
         }, rule)
      });

      me.reconfigure();
   },
   remove_rule: function (id) {
      var me = this;
      Ext.getCmp(me.props.panelRules).remove(Ext.getCmp(id));
      me.reconfigure();
   }
});
