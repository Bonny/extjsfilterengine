/* global Ext, data, Lang, readerStore_model, join, failureCallback, smu, rendererNotFound, Alert */

Ext.define("model.filters", {
  extend: "Ext.data.Model",
  idProperty: 'idFilter',
  fields: [
    {name: "idFilter", type: "int", defaultValue: -1},
    {name: "name", type: "string"},
    {name: "condition", type: "int", defaultValue: 0},
    {name: "status", type: "int", defaultValue: 1},
    {name: "apply_download", type: "int", defaultValue: 1},
    {name: "apply_after_sent", type: "int", defaultValue: 0},
    {name: "dataIns", type: "string"},
    {name: "dataMod", type: "string"},
    {name: "rules"},
    {name: "labels"},
    {name: 'checked', type: 'boolean', defaultValue: false},
    {name: 'mailboxes'},
    {name: 'type', type: "int", defaultValue: 0},
    {name: 'filterAction', type: 'string', defaultValue: ''},
    {name: 'priority', type: 'int', defaultValue: 0}
  ]
});

Ext.define('model.rule', {
  extend: 'Ext.data.Model',
  idProperty: 'id',
  fields: [
    {name: 'id', type: 'int'},
    {name: 'name', type: 'string'},
    {name: 'type', type: 'string'},
    {name: 'display', type: 'string'},
    {name: 'operators'},
    {name: 'values'}
  ]
});

Ext.define('model.operator', {
  extend: 'Ext.data.Model',
  idProperty: 'id',
  fields: [
    {name: 'id', type: 'int'},
    {name: 'display', type: 'string'},
    {name: 'name', type: 'string'}
  ]
});

Ext.define('model.values', {
  extend: 'Ext.data.Model',
  idProperty: 'key',
  fields: [
    {name: 'key', type: 'int'},
    {name: 'display', type: 'string'},
    {name: 'value', type: 'string'}
  ]
});

Ext.create("Ext.data.Store", {
  storeId: 'store-rule',
  model: 'model.rule',
  autoLoad: true,
  data: Ext.decode(smu.rule.config),
  proxy: {
    type: 'memory',
    reader: {
      type: 'json',
      root: 'rules'
    }
  }
});

Ext.define("combo.operator", {
  extend: 'Ext.form.ComboBox',
  alias: 'widget.combo-operator',
  fieldLabel: '',
  emptyText: '',
  value: '',
  displayField: 'display',
  valueField: 'id',
  multiSelect: false,
  editable: false,
  constructor: function (config) {
    var me = this;
    me.initConfig(extend({
      data: []
    }, config));
    me.store = Ext.create('Ext.data.Store', {
      storeId: new Date().getTime().toString(),
      model: 'model.operator',
      data: me.config.data,
      proxy: {
        type: 'memory',
        reader: {
          type: 'json'
        }
      }
    });
    me.callParent([config]);
  },
  listeners: {
    'select': function (combo, records, eOpts) {
      if (count(records) === 1) {
        log(records[0].data);
      }
    },
    'afterrender': function (combo, eOpts) {
      var value = combo.getValue();
      if (!Ext.isEmpty(value)) {
        var record = combo.getStore().getById(value);
        log("combo_operator#afterrender", record);
        if (!record)
          combo.setValue('');
      }
    }
  }
});

Ext.define("combo.values", {
  extend: 'Ext.form.ComboBox',
  alias: 'widget.combo-values',
  fieldLabel: '',
  emptyText: '',
  value: '',
  displayField: 'display',
  valueField: 'key',
  multiSelect: false,
  editable: false,
  constructor: function (config) {
    var me = this;
    me.initConfig(extend({
      data: []
    }, config));
    me.store = Ext.create('Ext.data.Store', {
      storeId: new Date().getTime().toString(),
      model: 'model.values',
      proxy: {
        type: 'memory',
        reader: {
          type: 'json'
        }
      },
      data: me.config.data
    });
    me.callParent([config]);
  },
  listeners: {
    'afterrender': function (combo, eOpts) {
      var value = combo.getValue();
      if (!Ext.isEmpty(value)) {
        var record = combo.getStore().getById(value);
        log("combo_value#afterrender", record);
        if (!record)
          combo.setValue('');
      }
    }
  }
});

Ext.apply(Ext.form.VTypes, {
  ruleweight: function (val, field) {
    return /^(100|[1-9]|[1-9][0-9]|0)$/g.test(val);
  },
  ruleweightText: Lang.get("label.err.ruleweight")
});

Ext.define('panel.rule', {
  extend: 'Ext.panel.Panel',
  alias: 'widget.panel-rule',
  layout: {
    type: 'hbox',
    pack: 'start',
    align: 'stretch'
  },
  border: false,
  bodyBorder: false,
  headerVisible: false,
  bodyPadding: 2,
  bodyStyle: "background:#DFE8F6",
  cls: 'x-panel-rule',
  setLastRule: function () {
    Ext.getCmp(this.props.btnLess).disable();
  },
  listeners: {
    'afterrender': function (panel) {
      setTimeout(function () {
        panel.setBodyStyle('background', '#FFFFFF');
      }, 200);
    }
  },
  getValue: function () {
    var me = this;
    return {
      's': (Ext.getCmp(me.props.cmbRule).getValue()).toString(),
      'o': (Ext.getCmp(me.props.cmbOperator).getValue()).toString(),
      'v': Ext.getCmp(me.props.valueObj).getValue(),
      'w': Ext.getCmp(me.props.txtWeight).getValue()
    };
  },
  constructor: function (config) {

    var me = this;
    me.initConfig(extend({
      counter: 0
    }, config));

    log(me.config);
    me.id = 'panel-rule-' + me.config.counter;

    me.props = {
      cmbRule: 'cmb-rule',
      cmbOperator: 'cmb-operator',
      btnPlus: 'btn-plus',
      btnLess: 'btn-less',
      panelValues: 'pnl-values',
      panelOperator: 'pnl-operator',
      valueObj: 'obj-value',
      txtWeight: 'txt-weight'
    };

    for (var i in me.props)
      me.props[i] = me.props[i] + "-" + me.config.counter;

    me.refresh = function (rule) {
      if (rule) {
        Ext.getCmp(me.props.panelOperator).updateField(rule.get('operators'));
        Ext.getCmp(me.props.panelValues).updateFieldByType(rule.get('type'), rule.get('values'));
      }
    };

    me.items = [{
        xtype: 'panel',
        border: false,
        bodyBorder: false,
        headerVisible: false,
        layout: {type: 'fit', pack: 'start', align: 'stretch'},
        flex: 0.1,
        margin: 2,
        hidden: true,
        items: {
          xtype: 'label',
          text: me.config.counter + "."
        }
      }, {
        xtype: 'panel',
        border: false,
        bodyBorder: false,
        headerVisible: false,
        layout: {type: 'fit', pack: 'start', align: 'stretch'},
        flex: 1.5,
        margin: 2,
        items: {
          xtype: 'combobox',
          submitValue: false,
          id: me.props.cmbRule,
          fieldLabel: '',
          emptyText: '',
          value: me.config.item.idRule,
          displayField: 'display',
          valueField: 'id',
          multiSelect: false,
          editable: false,
          store: Ext.data.StoreManager.lookup('store-rule'),
          listeners: {
            'select': function (combo, records, eOpts) {
              if (count(records) > 0)
                me.refresh(records[0]);
            },
            'afterrender': function (combo, eOpts) {
              var value = combo.getValue();
              if (!Ext.isEmpty(value))
                me.refresh(combo.getStore().getById(value));
            }
          }
        }
      }, {
        xtype: 'panel',
        layout: {type: 'fit', pack: 'start', align: 'stretch'},
        id: me.props.panelOperator,
        flex: 1.5,
        margin: 2,
        border: false,
        bodyBorder: false,
        headerVisible: false,
        updateField: function (data) {
          this.removeAll();
          this.insert(0, {
            xtype: 'combo-operator',
            submitValue: false,
            id: me.props.cmbOperator,
            data: data || [],
            value: me.config.item.idOperator
          });
        },
        items: {
          xtype: 'combo-operator',
          submitValue: false,
          id: me.props.cmbOperator,
          value: me.config.item.idOperator
        }
      }, {
        xtype: 'panel',
        id: me.props.panelValues,
        flex: 2,
        margin: 2,
        border: false,
        bodyBorder: false,
        headerVisible: false,
        layout: {type: 'fit', pack: 'start', align: 'stretch'},
        updateFieldByType: function (type, data) {
          var field = field = {
            xtype: 'textfield',
            submitValue: false,
            flex: 1,
            id: me.props.valueObj,
            value: me.config.item.value
          };
          if (type === 'COMBOBOX') {
            field.xtype = 'combo-values';
            field.data = data || [];
            field.value = parseInt(field.value);
          }
          this.removeAll();
          this.insert(0, field);
        },
        items: {
          id: me.props.valueObj,
          submitValue: false,
          flex: 1,
          xtype: 'textfield',
          value: me.config.item.value
        }
      }, {
        xtype: 'panel',
        border: false,
        bodyBorder: false,
        headerVisible: false,
        layout: {type: 'fit', pack: 'start', align: 'stretch'},
        flex: 0.3,
        margin: 2,
        items: {
          xtype: 'textfield',
          id: me.props.txtWeight,
          value: me.config.item.weight,
          submitValue: false,
          vtype: 'ruleweight',
          cls: 'filter-weight'
        }
      }, {
        xtype: 'panel',
        flex: 0.2,
        margin: 2,
        border: false,
        bodyBorder: false,
        headerVisible: false,
        items: {
          xtype: 'button',
          text: '+',
          id: me.props.btnPlus,
          width: 22,
          style: 'margin-left:2px',
          handler: function () {
            Ext.getCmp(me.config.parentId).add_rule(me);
            Ext.getCmp(me.props.btnLess).enable();
          }
        }
      }, {
        xtype: "panel",
        flex: 0.2,
        margin: 2,
        border: false,
        bodyBorder: false,
        headerVisible: false,
        items: {
          xtype: 'button',
          text: '-',
          id: me.props.btnLess,
          width: 22,
          disabled: me.config.counter === 0,
          handler: function () {
            Ext.getCmp(me.config.parentId).remove_rule(me.id);
          }
        }
      }];
    me.callParent([config]);
  }
});

Ext.define('panel.rules', {
  extend: 'Ext.form.Panel',
  alias: 'widget.panel-rules',
  counters: 0,
  layout: {type: "vbox", pack: 'start', align: 'stretch'},
  border: false,
  bodyBorder: false,
  headerVisible: false,
  height: 400,
  width: 710,
  defaults: {bodyPadding: 4},
  constructor: function (config) {

    var me = this, UID = new Date().getTime().toString();
    me.initConfig(extend({
    }, config));

    me.props = {
      panelRules: 'panel-rules'
    };

    for (var i in me.props)
      me.props[i] = me.props[i] + "-" + UID;

    var changeWeight = function (status) {
      Ext.iterate(Ext.ComponentQuery.query("textfield[cls~=filter-weight]"), function (i) {
        if (status)
          i.enable();
        else
          i.disable();
      });
    };

    var conditionChange = function (r, newValue, oldValue, eOpts) {
      if (!Ext.isEmpty(r.getSubmitValue())) {
        switch (parseInt(r.getSubmitValue())) {
          case 0:
            Ext.getCmp(me.props.panelRules).enable();
            changeWeight(false);
            break;
          case 1:
            Ext.getCmp(me.props.panelRules).enable();
            changeWeight(false);
            break;
          case 2:
            Ext.getCmp(me.props.panelRules).disable();
            changeWeight(false);
            break;
          case 3:
            Ext.getCmp(me.props.panelRules).enable();
            changeWeight(true);
            break;
          default:
            log("condition not valid");
            break;
        }
      }
    };

    me.items = [{
        xtype: 'hiddenfield',
        name: "idFilter"
      }, {
        xtype: 'panel',
        border: false,
        layout: "hbox",
        items: [{
            xtype: 'textfield',
            fieldLabel: Lang.get('label.filterName'),
            name: 'name',
            maxLength: 256,
            labelWidth: 90,
            //labelAlign: 'top',
            width: 160,
            allowBlank: false,
            flex: 0.8
          }, {
            xtype: 'tbspacer'
          }, {
            xtype: 'checkbox',
            labelWidth: 0,
            boxLabel: Lang.get('label.active'),
            name: 'status',
            inputValue: '1',
            style: 'margin-left:20px',
            flex: 0.5
          }, {
            xtype: 'tbspacer'
          }, {
            xtype: "numberfield",
            fieldLabel: Lang.get('label.priority'),
            name: 'priority',
            //maxValue: smu.config['filter.global.matching.priority.max'],
            maxValue: 100,
            labelWidth: 60,
            minValue: 0,
            flex: 0.5
          }]
      }, {
        xtype: 'checkboxgroup',
        hidden: true,
        labelWidth: 150,
        fieldLabel: Lang.get('label.whenApplyFilter'),
        columns: 1,
        vertical: true,
        items: [{
            boxLabel: Lang.get('label.downloadNewMail'), name: 'apply_download', inputValue: '1'
          }, {
            boxLabel: Lang.get('label.afterSent'), name: 'apply_after_sent', inputValue: '1'
          }]
      }
      , {
        xtype: 'panel',
        layout: {type: "hbox"},
        border: false,
        defaultType: 'radiofield',
        items: [{
            boxLabel: Lang.get("label.andCondition"), name: 'condition', inputValue: '0',
            listeners: {change: conditionChange, afterrender: conditionChange}
          }, {xtype: 'tbspacer', width: 6}, {
            boxLabel: Lang.get("label.orCondition"), name: 'condition', inputValue: '1',
            listeners: {change: conditionChange, afterrender: conditionChange}
          }, {xtype: 'tbspacer', width: 6}, {
            boxLabel: Lang.get("label.allCondition"), name: 'condition', inputValue: '2',
            listeners: {change: conditionChange, afterrender: conditionChange}
          }, {xtype: 'tbspacer', width: 6}, {
            boxLabel: Lang.get("label.useWeight"), name: 'condition', inputValue: '3',
            listeners: {change: conditionChange, afterrender: conditionChange}
          }]
      }, {
        xtype: 'panel',
        id: me.props.panelRules,
        border: false,
        headerVisible: false,
        overflowY: 'scroll',
        height: 200
      }];

    me.callParent([config]);

  },
  getRulesValue: function () {
    var children = Ext.getCmp(this.props.panelRules).items.items;
    var items = [];
    if (count(children) > 0) {
      Ext.iterate(children, function (c) {
        items.push(c.getValue());
      });
    }
    return items;
  },
  reconfigure: function () {

    var children = Ext.getCmp(this.props.panelRules).items.items;
    if (count(children) > 0) {

      for (var i = 0; i < count(children); i++)
        children[i].position = i;
      if (count(children) === 1)
        children[0].setLastRule();
    }
  },
  add_rule: function (beforeCmp, rule) {

    var me = this,
            position = 0;
    rule = rule || {};
    if (beforeCmp) {
      position = beforeCmp.position + 1;
    } else {
      // put in coda
      var children = Ext.getCmp(me.props.panelRules).items.items;
      if (count(children) > 0) {
        position = children[count(children) - 1].position + 1;
      }
    }

//      log("panel.rules#add_rule rule = {");
//      log(rule);
//      log("}")
//      log("panel.rules#add_rule [counters=" + me.counters + ", position=" + position + "]");

    var newID = me.counters++;
//      log("panel.rules#add_rule [newID=" + newID + "]");

    Ext.getCmp(me.props.panelRules).insert(position, {
      xtype: 'panel-rule',
      counter: newID,
      position: position,
      parentId: me.getId(),
      item: extend({
        idRule: '',
        idOperator: '',
        value: '',
        weight: ''
      }, rule)
    });

    me.reconfigure();

  },
  remove_rule: function (id) {
    var me = this;
    Ext.getCmp(me.props.panelRules).remove(Ext.getCmp(id));
    me.reconfigure();
  }
});

Ext.application({
  name: 'MyApp',
  launch: function () {
    Ext.create('Ext.window.Window', {
      title: "Test",
      autoShow: true,
      modal: true,
      bodyStyle: 'background-color:#FFF',
      height: 500,
      width: 720,
      layout: {
        type: "fit"
      },
      items: {
        xtype: 'panel-rules',
        padding: '10px',
        listeners: {
          afterLayout: function (that) {
            that.add_rule();      
//        if (count(filter.get('rules')) === 0) {
//          form.add_rule();
//        } else {
//          Ext.iterate(filter.get('rules'), function (r) {
//            form.add_rule(null, {
//              idRule: r['subject'],
//              idOperator: r['operator'],
//              value: r['value'],
//              weight: r['weight']
//            });
//          });
//        }
          }
        }
      },
      buttons: [{
          xtype: 'button',
          text: Lang.get("label.save"),
          tooltip: Lang.get("label.save"),
          width: 80,
          style: 'margin-left:10px;',
          handler: function () {
            log("try submit form filter");
            var me = this;
            var win = this.up('window');
            var formCmp = Ext.getCmp(win.getInitialConfig('formId'));
            log(formCmp.getRulesValue());
          }
        }]
    });
  }
});